package de.quh.event;

import de.quh.Sakura;

/**
 * This is the basis for the events.
 * @author Quh
 */
public class Event {
	
	/**
	 * Shows if the Event is cancelled.
	 */
	private boolean cancelled;
	
	/**
	 * Method to register this Event
	 */
	public void call(){
		Sakura.instance.modManager.getMods().stream().filter(mod -> mod.isEnabled()).forEach(mod -> mod.event(this));
	}
	
	/**
	 * @return Cancel-State of the Event.
	 */
	public boolean isCancelled() {
		return cancelled;
	}
	
	/**
	 * Sets if the Event is cancelled.
	 */
	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}

}
