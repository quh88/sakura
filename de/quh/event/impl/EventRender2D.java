package de.quh.event.impl;

import de.quh.event.Event;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.ScaledResolution;

public class EventRender2D extends Event{
	
	/**
	 * Fontrenderer of Minecraft.
	 */
	private FontRenderer fontRenderer;
	
	/**
	 * ScaledResolution of Minecraft.
	 */
	private ScaledResolution scaledResolution;
	
	/**
	 * Constructor of this Event.
	 * Called in GuiIngame.java#renderGameOverlay
	 */
	public EventRender2D(FontRenderer fontRenderer, ScaledResolution scaledResolution){
		this.fontRenderer = fontRenderer;
		this.scaledResolution = scaledResolution;
	}
	
	/**
	 * @return The FontRenderer.
	 */
	public FontRenderer getFontRenderer() {
		return fontRenderer;
	}
	
	/**
	 * @return The ScaledResolution.
	 */
	public ScaledResolution getScaledResolution() {
		return scaledResolution;
	}
}
