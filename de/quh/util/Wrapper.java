package de.quh.util;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.settings.GameSettings;

/**
 * This is a class to get access to all Minecraft methods.
 * @author Quh
 */

public class Wrapper {

	private static Minecraft mc = Minecraft.getMinecraft();
	
	/**
	 * @return the local player.
	 */
	public static EntityPlayerSP player(){
		return mc.thePlayer;
	}
	
	/**
	 * @return the gamesettings.
	 */
	public static GameSettings keyboard(){
		return mc.gameSettings;
	}

}
