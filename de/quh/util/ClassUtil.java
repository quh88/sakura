package de.quh.util;

import java.util.ArrayList;
import java.util.Arrays;

import com.google.common.collect.Lists;
import com.google.common.primitives.Ints;
/**
 * This class contains method used for classes.
 * @author Quh
 */
public class ClassUtil {
	
	/**
	 * Returns all indexes of a char in a string.
	 */
	public static int[] indexesOfChar(String string, char character){
		ArrayList<Integer> indexes = Lists.newArrayList();
		for(int i = 0; i < string.length(); i++){
			if(string.charAt(i) == character){
				indexes.add(i);
			}
		}
		int[] array = Ints.toArray(indexes);
		Arrays.sort(array);
		return array;
	}

}
