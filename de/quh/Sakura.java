package de.quh;

import org.lwjgl.opengl.Display;

import de.quh.mod.ModManager;
import de.quh.mod.impl.visual.HUD;

/**
 * This is the main class of the cheat.
 * This is used to gain access to backend stuff, like the ModuleManager or Cheat Informations.
 * @author Quh
 */
public class Sakura {
	
	/**
	 * Static instance to access the loaded backend classes.
	 */
	public static Sakura instance;

	/**
	 * Name of the Cheat.
	 */
	public final String CHEAT_NAME = "Sakura";
	
	/**
	 * Version of the Cheat. (Changes constantly)
	 */
	public final float CHEAT_VERSION = 0.1F;
	
	/**
	 * Manager of the Modules.
	 */
	public ModManager modManager;
	
	/**
	 * Loads/Initializes all backend stuff.
	 * Called in Minecraft.java#startGame
	 */
	public void start(){
		//Initialize the instance
		this.instance = this;
		//Set Title of the Window
		Display.setTitle(this.CHEAT_NAME + " v" + this.CHEAT_VERSION);
		//Initialize the ModManager
		(this.modManager = new ModManager()).init();
	}
}
