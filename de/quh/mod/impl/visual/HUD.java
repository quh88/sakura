package de.quh.mod.impl.visual;

import de.quh.Sakura;
import de.quh.event.Event;
import de.quh.event.impl.EventRender2D;
import de.quh.mod.Mod;

public class HUD extends Mod{

	@Override
	public void enable() {
		
	}

	@Override
	public void disable() {
		
	}

	@Override
	public void event(Event event) {
		if(event instanceof EventRender2D){
			EventRender2D renderEvent = (EventRender2D) event;
			renderEvent.getFontRenderer().drawStringWithShadow(Sakura.instance.CHEAT_NAME, 1, 1, 0xFFFFFFFF);
		}
	}

	@Override
	public void run() {
		
	}

}
