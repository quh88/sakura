package de.quh.mod.impl.movement;

import org.lwjgl.input.Keyboard;

import de.quh.event.Event;
import de.quh.mod.Mod;
import de.quh.util.Wrapper;

/**
 * This is a Module that will automatically press the sprint keybind.
 * @author Quh
 */
public class Sprint extends Mod{

	@Override
	public void enable() {
		System.out.println("Sprint was enabled");
	}

	@Override
	public void disable() {
		
	}

	@Override
	public void run() {
		Wrapper.keyboard().keyBindSprint.setKeyBindState(Wrapper.keyboard().keyBindSprint.getKeyCode(), true);
	}

	@Override
	public void event(Event event) {
		
	}

}
