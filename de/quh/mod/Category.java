package de.quh.mod;
/**
 * The category that a Module falls into.
 * @author Quh
 */
public enum Category {

	COMBAT, MISC, MOVEMENT, VISUAL;
	
}
