package de.quh.mod;

import java.util.List;

import com.google.common.collect.Lists;

import de.quh.mod.impl.movement.Sprint;
import de.quh.mod.impl.visual.HUD;

/**
 * This is a Manager for all Modules of the Cheat.
 * @author Quh
 */
public class ModManager {
	/**
	 * List of all Modules.
	 */
	private List<Mod> mods;
	
	/**
	 * Initializes the mod list.
	 */
	public void init(){
		this.mods = Lists.newArrayList();
		this.mods.add(new Sprint());
		this.mods.add(new HUD());
	}
	
	/**
	 * @return The list of all mods
	 */
	public List<Mod> getMods() {
		return mods;
	}
	
	/**
	 * @return The Mod that were searched for by name.
	 */
	public Mod getMod(String name){
		return this.mods.stream().filter(mod -> mod.getName().equalsIgnoreCase(name)).findFirst().orElse(null);
	}
	
	/**
	 * @return The Mod that were searched for by class.
	 */
	public Mod getMod(Class<?> modClass){
		return this.mods.stream().filter(mod -> mod.getClass().equals(modClass)).findFirst().orElse(null);
	}

}
