package de.quh.mod;

import java.util.Arrays;

import org.lwjgl.input.Keyboard;

import de.quh.event.Event;
import de.quh.util.ClassUtil;

/**
 * This is the basis for the modules.
 * @author Quh
 */
public abstract class Mod {
	/**
	 * Name of the Module.
	 */
	private String name;
	
	/**
	 * Key to toggle the Module.
	 */
	private int key;
	
	/**
	 * Category of the Module
	 */
	private Category category;
	
	/**
	 * Shows if the Module is enabled.
	 */
	private boolean enabled;
	
	/**
	 * Constructor of the Module.
	 */
	public Mod(){
		int[] indexesOfPoint = ClassUtil.indexesOfChar(this.getClass().getName(), '.');
		this.name = this.getClass().getName().substring(indexesOfPoint[indexesOfPoint.length - 1] + 1);
		this.key = Keyboard.KEY_NONE;
		String categoryName = this.getClass().getName().substring(indexesOfPoint[indexesOfPoint.length - 2] + 1);
        Arrays.stream(Category.values()).filter(category -> category.name().equalsIgnoreCase(categoryName)).forEach(category -> this.category = category);
	}
	
	/**
	 * Toggles the Module
	 */
	public void toggle(){
		this.enabled = !this.enabled;
		if(this.enabled){
			this.enable();
		}else{
			this.disable();
		}
	}
	
	/**
	 * Method that will be executed once at toggling on the Module.
	 */
	public abstract void enable();
	
	/**
	 * Method that will be executed once at toggling off the Module.
	 */
	public abstract void disable();
	
	/**
	 * Method that will be executed constantly at a setted pont.
	 */
	public abstract void event(Event event);
	
	/**
	 * Method that will be executed constantly if the Module is toggled on.
	 * Called in EntityPlayerSP.java#onUpdate
	 */
	public abstract void run();
	
	/**
	 * @return Name of the Module.
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * @return Key of the Module.
	 */
	public int getKey() {
		return this.key;
	}
	
	/**
	 * @return Category of the Module.
	 */
	public Category getCategory() {
		return this.category;
	}
	
	/**
	 * @return State of the Module.
	 */
	public boolean isEnabled() {
		return enabled;
	}

}
